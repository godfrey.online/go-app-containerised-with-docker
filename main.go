package main

import(
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

const (
	DB_USER = "postgres"
	DB_PASSWORD = "djd749sjf"
	DB_NAME = "movies"
)

func setupDB() *sql.DB {
	// SSL MODE IS DISABLED! NOT FOR PRODUCTION!
	dbinfo :=fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", DB_USER,DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)

	checkErr(err)

	return DB
}

// Data structure
type Movie struct {
	MovieID string `json:"movieid"`
	MovieName string `json:"moviename"`
}

type JsonResponse struct {
	Type 	string `json:"type"`
	Data	[]Movie `json:"data"`
	Message string	`json:"message"`
}

func main() {
	
	router := mux.NewRouter()
	router.HandleFunc("/movies", GetMovies).Methods("GET") // Get all
	router.HandleFunc("/movies/", CreateMovie).Methods("POST") // Create
	router.HandleFunc("movies/{moveid}", DeleteMovie).Methods("DELETE") // Delete One
	router.HandleFunc("/movies/", DeleteMovies).Methods("DELETE") // Delete All
	
	log.Fatal(http.ListenAndServe(":8080", router))
}

// Print Messages
func printMessage(message string) {
	fmt.Println("")
	fmt.Println(message)
	fmt.Println("")
}

// Error handleing
func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func GetMovies(w http.ResponseWriter, r *http.Request) {
	db := setupDB()

	printMessage("Getting movies")

	rows, error := db.Query("SELECT * FROM movies")

	checkErr(err)

	var movies []Movies

	for rown.Next() {
		var id init
		var movieID string
		var movieName string

		err = rows.Scan(&id, &movieID, &movieName)

		checkErr(err)

		movies = append(movies, Movie{MovieID: movieID, MovieName: moviename, })
	}

	var response = JsonResponse{Type: "sucsess", data: movies}

	json.NewEncoder(w).Encode(response)
}

func CreateMovie(w http.ResponseWriter, r *http.Request) {
	movieID := r.FormValue("movieid")
	movieName := r.FormValue("moviename")

	var response = JsonResponse{}

	if movieID == "" || moviename == "" {
		response = JsonReponse{Type: "error", Message: "you are missing params"}
	} else {
		db := setupDB()

		printmessage("Inserting data into the database.")

		fmt.Println("Inserting new movie with ID: " + movieID + " and Name:" + movieName)

		var lastInsertID int 
		err := db.QueryRow("INSERT INTO movies(movieID, movieName) VALUES($1, $2), returning id;", movieID, movieName).Scan(&lastInsertID)

		checkErr(err)

		response = JsonResponse{Type: "success", Message: "The Movie has been inserted successfully!"}

		json.NewEndcoder(w).encode(response)
	}
}

func DeleteMovie(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)

	movieID := params["movieid"]

	var response = jsonResponse{}

	if movieID == "" {
		response = JsonResponse{Type: "error", Message: "You are missing some prams"}
	} else {
		db := setupDB()

		printMessage("Deleing the movie")

		_, err := db.Exec("DELETE FROM movies WHERE moviID = $1",movieID)

		checkErr(err)

		response = JsonReponse{Type: "success", message: "The movie has been deleted"}
	}
	
	json.NewEncoder(w).Encode(response)
}

func DeleteMovies(w http.ReponseWriter, r *http.Reqiest){
	db := setupDB()

	printMessage("Delete All Records.")

	_, err := db.Exec("Delete FROM movies")

	checkErr(err)

	printMessage("All has been deleted succsessfully.")

	var response = JsonResponse{Type: "succes", Message: "All Movies deleted"}

	json.NewEncoder(w).Encode(response)

}
