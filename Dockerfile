FROM golang:1.15.6-alpine3.12

WORKDIR /app
#ADD go.mod go.sum  ./
#RUN go mod download

COPY . .

RUN go run main.go

CMD [ "go run main.go" ]
